# From 7.2 to 7.3

# GitLab 7.3 has not been released yet!

This document currently just serves as a place to keep track of updates that will be needed for the 7.3 update.

### Update config files

* HTTP setups: Make `/etc/nginx/sites-available/nginx` the same as https://gitlab.com/gitlab-org/gitlab-ce/blob/7-0-stable/lib/support/nginx/gitlab but with your settings.
* HTTPS setups: Make `/etc/nginx/sites-available/nginx-ssl` the same as https://gitlab.com/gitlab-org/gitlab-ce/blob/7-0-stable/lib/support/nginx/gitlab-ssl but with your setting

### Configure Redis to use sockets

    # Configure redis to use sockets
    sudo cp /etc/redis/redis.conf /etc/redis/redis.conf.orig
    sed -e 's/^# unixsocket /unixsocket /' -e 's/^port .*/port 0/' /etc/redis/redis.conf.orig | sudo tee /etc/redis/redis.conf
    # Activate the changes to redis.conf
    sudo service redis-server restart
    # Add git to the redis group
    sudo usermod -aG redis git

    cd /home/git/gitlab
    # Configure Redis connection settings
    sudo -u git -H cp config/resque.yml.example config/resque.yml
    # Change the Redis socket path if necessary
    sudo -u git -H editor config/resque.yml

    cd /home/git/gitlab-shell
    # Configure gitlab-shell to use Redis sockets
    sudo -u git -H sed -i 's|^  # socket.*|  socket: /var/run/redis/redis.sock|' config.yml

